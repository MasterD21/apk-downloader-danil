﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using admob;
using System;
using System.Linq;

public class ADManager : MonoBehaviour {

    public static ADManager Instance { get; set; }
	// Use this for initialization
	void Start () {
        Instance = this;
        DontDestroyOnLoad(gameObject);
#if UNITY_EDITOR
#elif UNITY_ANDROID

        Admob.Instance().initAdmob(GlobalStrings.GetString("ADBannerID"), GlobalStrings.GetString("ADFullID"));

        // Admob.Instance().setTesting(true);
        Admob.Instance().loadInterstitial();

        Admob.Instance().loadRewardedVideo(GlobalStrings.GetString("ADVideoReward"));
        Admob.Instance().rewardedVideoEventHandler += AdManager_rewardedVideoEventHandler;
#endif
    }



    public void ShowRewardVideo()
    {
#if UNITY_EDITOR
        RollReward();
#elif UNITY_ANDROID
        if (Admob.Instance().isRewardedVideoReady())
        {
            Admob.Instance().showRewardedVideo();
            //RollReward();
            Admob.Instance().loadRewardedVideo(GlobalStrings.GetString("ADVideoReward"));
        }
        else
        {
            Admob.Instance().loadRewardedVideo(GlobalStrings.GetString("ADVideoReward"));
        }
#endif
    }
    private void RollReward()
    {
        DateTime dateTime = DateTime.Now;
        dateTime = dateTime.AddHours(
            Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeHours")) * -1
        );
        dateTime = dateTime.AddMinutes(
            Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeMinutes")) * -1
        );
        if (dateTime > SaveManager.save.lastRoll || SaveManager.save.AdditionalRoll)
        {
            //--- Check & Update conditions ----
            if (dateTime > SaveManager.save.lastRoll)
            {
                SaveManager.save.AdditionalRoll = true;

                SaveManager.save.lastRoll = DateTime.Now;
            }
            else
            {
                SaveManager.save.AdditionalRoll = false;
            }
            //-----------------------------------


            //------ Do "Roll" code -------

            // Check if user used all rewards => refresh list of rewards
            if (SaveManager.save.rewards.Count == GlobalStrings.rewardElements.Count)
            {
                SaveManager.save.rewards = new List<int>();
            }
            // all not used Rewards
            int[] ids =
                GlobalStrings.rewardElements.Where(x => !SaveManager.save.rewards.Contains(x.id)).Select(x => x.id).ToArray();

            // id of random reward
            System.Random rnd = new System.Random();
            int id_prize = ids[rnd.Next(0, ids.Length)];

            // add selected reward to SAVE
            SaveManager.save.rewards.Add(id_prize);
            // Save data
            

            // Set Param to Global Data
            try
            {
                GlobalStrings.idReward = GlobalStrings.rewardElements.IndexOf(GlobalStrings.rewardElements.First(x => x.id == id_prize));

            }
            catch (Exception)
            {
                if (SaveManager.save.rewards.Count >= 1)
                {
                    try
                    {
                        GlobalStrings.idReward = GlobalStrings.rewardElements.IndexOf(GlobalStrings.rewardElements.First(x => x.id == SaveManager.save.rewards[SaveManager.save.rewards.Count - 1]));
                    }
                    catch (Exception)
                    {

                        GlobalStrings.idReward = 0;
                    }
                }
                else
                {
                    GlobalStrings.idReward = -1;
                }
            }
            //-----------------------------


        }
    }
    private void AdManager_rewardedVideoEventHandler(string eventName, string msg)
    {
        if (eventName == AdmobEvent.onRewarded)
        {
            RollReward();
            Debug.Log("Rewarding player...rewarded count" + msg);
        }
        //Debug.Log("Event: " + eventName);
    }
    public void ShowBaner(AdSize size, int position, int margin = 0, string instanceName = "Banner")
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        Admob.Instance().showBannerRelative(size, position, margin, instanceName);
#endif
    }
    public void ShowInterstitial()
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        if (Admob.Instance().isInterstitialReady())
        {
            Admob.Instance().showInterstitial();
            Admob.Instance().loadInterstitial();
        }
        else
        {
            Admob.Instance().loadInterstitial();
            StartCoroutine(W8ter());
            Admob.Instance().showInterstitial();
        }
        SaveManager.SaveGame();
#endif
    }

    IEnumerator W8ter()
    {
        //we w8 3 sec
        yield return new WaitForSeconds(3);
    }
}
