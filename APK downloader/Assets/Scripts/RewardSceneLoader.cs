﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RewardSceneLoader : MonoBehaviour {
    public Text rollLable;
    public Text rerollLable;
    public GameObject panelRoll;
    public GameObject panelReroll;
    public Text itemName;
    public Text takeReward;
    public Image itemSprite;
    public Text timeToNext;
    public GameObject timeToRoll;
    public GameObject buttonRoll;
    public GameObject buttonReRoll;
    public GameObject lableItem;
    public GameObject imageItem;
    
    
    // Use this for initialization
    void Start ()
    {


        ADManager.Instance.ShowBaner(admob.AdSize.MediumRectangle, admob.AdPosition.BOTTOM_CENTER, 5);
        rollLable.text = GlobalStrings.GetString("RewardRoll");
        rerollLable.text = GlobalStrings.GetString("RewardReRoll");
        takeReward.text = GlobalStrings.GetString("RewardTakeReward");


    
    }
    
    public void ShowVideo()
    {
        ADManager.Instance.ShowRewardVideo();
    }

    
    public void DownloadReward()
    {
        if (GlobalStrings.rewardElements[GlobalStrings.idReward].file_link.Contains("http"))
        {
            Debug.Log("Take Prize URL");
            Application.OpenURL(GlobalStrings.rewardElements[GlobalStrings.idReward].file_link);
        }
        else
        {
            #region Download file from asset to local
            // Put your file to "YOUR_UNITY_PROJ/Assets/StreamingAssets"
            // example: "YOUR_UNITY_PROJ/Assets/StreamingAssets/db.bytes"

            string realPath = Application.persistentDataPath + "/" + GlobalStrings.rewardElements[GlobalStrings.idReward].file_link;
            if (!File.Exists(realPath))
            {

                // Android
                string oriPath = Path.Combine(Application.streamingAssetsPath, GlobalStrings.rewardElements[GlobalStrings.idReward].file_link);

                // Android only use WWW to read file
                WWW reader = new WWW(oriPath);
                while (!reader.isDone) { }

                File.WriteAllBytes(realPath, reader.bytes);
                
            }
            #endregion
            

            Application.OpenURL(realPath);
        }
    }
	// Update is called once per frame
	void Update () {

        DateTime dateTime = DateTime.Now;
        dateTime = dateTime.AddHours(
            Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeHours")) * -1
        );
        dateTime = dateTime.AddMinutes(
            Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeMinutes")) * -1
        );
        //Debug.Log(
        //        string.Format("<color=red>TimeParams:</color>  dateTime={0: H::mm::ss}, RewardWaitTimeHours={1}, RewardWaitTimeMinutes={2}",
        //        SaveManager.save.lastRoll,
        //    Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeHours")) * -1,
        //    Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeMinutes")) * -1
        //        )
        //        );
        // Check if user roll something before
        if (SaveManager.save.rewards.Count < 1)
        {
            //Debug.Log(
            //    string.Format("<color=red>LOW rewards:</color>  last_roll={0: H::mm::ss}, SaveManager.save.rewards.Count={1}",
            //    SaveManager.save.lastRoll,
            //    SaveManager.save.rewards.Count
            //    )
            //    );
            // If no roll before
            panelRoll.SetActive(true);
            panelReroll.SetActive(false);
            timeToRoll.SetActive(false);
            rollLable.text = GlobalStrings.GetString("RewardRoll");
            buttonRoll.SetActive(true);
            buttonReRoll.SetActive(false);
        }
        else
        {
            // if user get some rewards
            //Debug.Log(
            //    string.Format("<color=yellow>Count>1 rewards:</color> dateTime = {0: H::mm::ss}",
            //    dateTime
            //    )
            //    );
            panelRoll.SetActive(false);
            panelReroll.SetActive(true);
            buttonRoll.SetActive(false);
            buttonReRoll.SetActive(true);

            timeToRoll.SetActive(false);
            //---------------
            // display last rolled item
            if (GlobalStrings.idReward >= 0)
            {
                itemName.text = GlobalStrings.rewardElements[GlobalStrings.idReward].name;
                itemSprite.sprite = GlobalStrings.rewardElements[GlobalStrings.idReward].img.img;
                lableItem.SetActive(true);
                imageItem.SetActive(true);
            }
            else
            {
                lableItem.SetActive(false);
                imageItem.SetActive(false);
            }
            
            //---------------

            // Lets check if roll avalible
            // if time of last roll > time now OR yser got additional roll
            if (dateTime > SaveManager.save.lastRoll || SaveManager.save.AdditionalRoll)
            {
                //Debug.Log(
                //string.Format("<color=gray>Conditions skip rewards:</color>"
                //)
                //);
                if (dateTime > SaveManager.save.lastRoll)
                {
                    // if time to last roll >> time now + delta
                    rerollLable.text = GlobalStrings.GetString("RewardRoll");

                    //Debug.Log(
                    //    string.Format("<color=green>conditions dateTime:</color>  last_roll={0: H::mm::ss}, SaveManager.save.rewards.Count={1}, AdditionalRoll={2}",
                    //    SaveManager.save.lastRoll,
                    //    SaveManager.save.rewards.Count,
                    //    SaveManager.save.AdditionalRoll
                    //    )
                    //    );
                }
                else
                {
                    //Debug.Log(
                    //    string.Format("<color=green>conditions AdditionalRoll:</color>  last_roll={0: H::mm::ss}, SaveManager.save.rewards.Count={1}, AdditionalRoll={2}",
                    //    SaveManager.save.lastRoll,
                    //    SaveManager.save.rewards.Count,
                    //    SaveManager.save.AdditionalRoll
                    //    )
                    //    );
                    rerollLable.text = GlobalStrings.GetString("RewardReRoll");
                }
            }
            else
            {
                //Debug.Log(
                //string.Format("<color=pink>Conditions false rewards:</color>"
                //)
                //);
                buttonRoll.SetActive(false);
                buttonReRoll.SetActive(false);
                // if user spend all rolls 
                DateTime time = DateTime.Now;
                DateTime tmp = SaveManager.save.lastRoll;

                tmp = tmp.AddHours(
                    Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeHours"))
                );
                tmp = tmp.AddMinutes(
                    Convert.ToInt32(GlobalStrings.GetString("RewardWaitTimeMinutes"))
                );
                timeToRoll.SetActive(true);
                timeToNext.text = string.Format("{0}: {1: HH::mm::ss}", GlobalStrings.GetString("RewardWaitReward"), tmp - time);
            }

        }
        #region oldCode
        //if (dateTime > SaveManager.save.lastRoll)
        //{
        //    panelRoll.SetActive(true);
        //    panelReroll.SetActive(false);
        //    timeToRoll.SetActive(false);

        //}
        //else
        //{ 
        //    if(!SaveManager.save.AdditionalRoll)
        //    {
        //        reRollBTN.SetActive(false);
        //    }
        //    else
        //    { reRollBTN.SetActive(true); }

        //    id_prize = GlobalStrings.rewardElements[GlobalStrings.rewardElements.Count - 1].id;


        //    try
        //    {
        //        GlobalStrings.idReward = GlobalStrings.rewardElements.IndexOf(GlobalStrings.rewardElements.First(x => x.id == id_prize));

        //    }
        //    catch (Exception) { GlobalStrings.idReward = 0; }

        //    itemName.text = GlobalStrings.rewardElements[GlobalStrings.idReward].name;
        //    itemSprite.sprite = GlobalStrings.rewardElements.First(x => x.id == id_prize).img.img;

        //}
        #endregion
    }
}
