﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour {

    public void changeScreen(string screenName)
    {
        //admob.Admob.Instance().removeBanner();
        admob.Admob.Instance().removeBanner();


        SceneManager.LoadScene(screenName);

#if UNITY_EDITOR
#elif UNITY_ANDROID
        if (admob.Admob.Instance().isInterstitialReady())
        {
            admob.Admob.Instance().showInterstitial();
            admob.Admob.Instance().loadInterstitial();
        }
        else
        {
            admob.Admob.Instance().loadInterstitial();
            StartCoroutine(W8ter());
            admob.Admob.Instance().showInterstitial();
        }
#endif
        SaveManager.SaveGame();
    }

    IEnumerator W8ter()
    {
        //we w8 3 sec
        yield return new WaitForSeconds(3);
    }
    public void changeScreenInfo(string screenName)
    {
        GlobalStrings.isInfo = true;
        changeScreen(screenName);
    }
    public void changeScreenGuide(string screenName)
    {
        GlobalStrings.isInfo = false;
        changeScreen(screenName);
    }
}
