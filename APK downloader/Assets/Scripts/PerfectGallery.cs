﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PerfectGallery : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{

    
    public PerfectImage[] images;


    private Image prev;
    private Image current;
    private Image next;

    private GameObject nextObj;
    private GameObject currentObj;
    private GameObject prevObj;

    private Vector3 positionCenter;

    private bool isActive = true;
    [Tooltip("Percent of half width witch blabla")]
    [SerializeField]
    private float percentSwap = .1f;
    private float sizeSwap = .0f;

    private int currentImage = 0;

    private Vector3 currPos;
    private Vector3 nextPos;
    private void Awake()
    {

        //current.sprite = ;
        //Debug.Log(string.Format("posChild: X={0} Y={1} Z={2}",
        //    Container.GetComponent<RectTransform>().position.x,
        //    Container.GetComponent<RectTransform>().position.y,
        //    Container.GetComponent<RectTransform>().position.z
        //    ));
    }

    private void Start()
    {
        float prevScale = 1;
        
        #region init Prev Cur Next Objects
        gameObject.AddComponent<RectMask2D>();

        nextObj = new GameObject("SecondContainer");
        nextObj.transform.parent = this.transform;
        next = nextObj.AddComponent<Image>();
        next.preserveAspect = true;

        prevScale = nextObj.transform.localScale.x;
        nextObj.GetComponent<RectTransform>().sizeDelta = new Vector2(Get_Width(this), Get_Height(this) );
        nextObj.transform.position = new Vector3(
            this.GetComponent<RectTransform>().position.x,
            this.GetComponent<RectTransform>().position.y,
            this.GetComponent<RectTransform>().position.z
            );
        nextPos = new Vector3(
            this.GetComponent<RectTransform>().position.x,
            this.GetComponent<RectTransform>().position.y,
            this.GetComponent<RectTransform>().position.z
            );
        nextObj.SetActive(false);
        nextObj.transform.localScale = new Vector3(1, 1, 1);


        currentObj = new GameObject("Container");
        currentObj.transform.parent = this.transform;
        current = currentObj.AddComponent<Image>();
        current.preserveAspect = true;

        currentObj.GetComponent<RectTransform>().sizeDelta = new Vector2(Get_Width(this), Get_Height(this));
        currentObj.transform.position = new Vector3(
            this.GetComponent<RectTransform>().position.x,
            this.GetComponent<RectTransform>().position.y,
            this.GetComponent<RectTransform>().position.z
            ); 
        currPos= new Vector3(
             this.GetComponent<RectTransform>().position.x,
             this.GetComponent<RectTransform>().position.y,
             this.GetComponent<RectTransform>().position.z
             );
        currentObj.transform.localScale = new Vector3(1, 1, 1);

        positionCenter = Camera.main.ScreenToWorldPoint(
            this.GetComponent<RectTransform>().position
            );

        prevObj = new GameObject("PrevContainer");
        prevObj.transform.parent = this.transform;
        prev = prevObj.AddComponent<Image>();
        prev.preserveAspect = true;
        prev.maskable = true;



        prevObj.GetComponent<RectTransform>().sizeDelta = new Vector2(Get_Width(this), Get_Height(this));
        prevObj.transform.localScale = new Vector3(1, 1, 1);
        prevObj.transform.position = new Vector3(
            this.GetComponent<RectTransform>().position.x - Get_Width(currentObj)/prevScale,
            this.GetComponent<RectTransform>().position.y,
            this.GetComponent<RectTransform>().position.z
            );
        sizeSwap = (Get_Width(currentObj) / prevScale) * percentSwap;
        #endregion



        if (images != null)
        {
            foreach(PerfectImage img in images)
            {
                img.Load();
            }
        }
        if(images == null || images.Length == 0)
        {
            images = new PerfectImage[] { new PerfectImage() };
            images[0].Load();
        }

        current.sprite = SetImage(currentImage);//images[0].getImage();
        next.sprite = SetImage(currentImage + 1);//images[1].getImage();
        prev.sprite = SetImage(currentImage - 1);//images[1].getImage();
        //gameObject.AddComponent<SpriteRenderer>();
    }

   

    // Update is called once per frame
    void Update () {
        //SpriteRenderer renderer = gameObject.AddComponent<SpriteRenderer>();
        //Transform child = gameObject.transform.Find("Container");
        //child.transform.localScale = new Vector3(1,0.5f);

        ////child.position.Set(0f, -sizePanel, 0f);
        //child.position = this.transform.position;
        //child.position.Set(
        //    this.transform.position.x,
        //    this.transform.position.y + sizePanel,
        //    this.transform.position.z
        //    );
        //Image renderer = child.GetComponent<Image>();
        
       

    }


    #region Rect width height
    public static Rect Get_Rect(GameObject gameObject)
    {
        if (gameObject != null)
        {
            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();

            if (rectTransform != null)
            {
                return rectTransform.rect;
            }
        }
        else
        {
            //Debug.Log("Game object is null.");
        }

        return new Rect();
    }


    public static float Get_Width(Component component)
    {
        if (component != null)
        {
            return Get_Width(component.gameObject);
        }

        return 0;
    }
    public static float Get_Width(GameObject gameObject)
    {
        if (gameObject != null)
        {
            var rect = Get_Rect(gameObject);
                return rect.width;
        }

        return 0;
    }


    public static float Get_Height(Component component)
    {
        if (component != null)
        {
            return Get_Height(component.gameObject);
        }

        return 0;
    }
    public static float Get_Height(GameObject gameObject)
    {
        if (gameObject != null)
        {
            var rect = Get_Rect(gameObject);
            return rect.height;
        }

        return 0;
    }
    #endregion

    private Sprite SetImage(int position = 0)
    {
        int pos = 0;
        pos = (position) % images.Length;
        if (pos < 0)
        {
            pos = images.Length + pos;
        }

        

        //Debug.Log(string.Format(
        //     "<color=gray> Position: </color> pos[{0}] currentImage[{1}]",
        //     pos,
        //     currentImage
        //     ));
        return images[pos].getImage();
    }

    private void Increment()
    {

        currentImage = (currentImage + 1) % images.Length;
        if (currentImage < 0)
        {
            currentImage = images.Length + currentImage;
        }

    }
    private void Decrement()
    {

        currentImage = (currentImage - 1) % images.Length;
        if (currentImage < 0)
        {
            currentImage = images.Length + currentImage;
        }

    }
    private Vector3 mousePos;
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnBeginDrag");
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        //Debug.Log(string.Format(
        //    "<color = brown> Position: </color> X[{0}] Y[{1}]",
        //    positionCenter.x,
        //    positionCenter.y
        //    ));
        //Debug.Log(string.Format(
        //     "<color = gray> Position: </color> X[{0}] Y[{1}]",
        //     mousePosBegin.x,
        //     mousePosBegin.y
        //     ));
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        #region prevCode dont use it
        //Vector3 positionCurrent = Camera.main.ScreenToWorldPoint(
        //    currentObj.GetComponent<RectTransform>().position
        //    );

        //Vector3 positionPrev = Camera.main.ScreenToWorldPoint(
        //    prevObj.GetComponent<RectTransform>().position
        //    );
        ///////////////////////
        // Catch swipe to left
        ///////////////////////
        //
        // If position of current Image < ( Position of Center Galler * (100% - Percent) ):
        //       Change layouts;
        //
        // Precent:
        // 
        // L         P    C             R
        // 
        // L - left side
        // R - right side
        // C - center
        // P - position whitch calc as Distance betwen L and C multiple on percent
        ////////////////////////

        //if (positionCurrent.x<(positionCenter.x * (1.0f - percentSwap)))
        //{
        //    //Debug.Log(string.Format(
        //    //    "<color=orange>Coords:</color> positionCenter.x={0} positionCurrent.x={1}  current.sprite.name = {2}",
        //    //    positionCenter.x,
        //    //    positionCurrent.x,
        //    //    current.sprite.name
        //    //    ));

        //    Increment();

        //    current.sprite = SetImage(currentImage);
        //    next.sprite = SetImage(currentImage+1);
        //    prev.sprite = SetImage(currentImage-1);




        //}

        /////////////////////////
        //// Catch swipe to right
        /////////////////////////
        ////
        //// If position of current Image > ( Position of Center Galler * (Percent) ):
        ////       Change layouts;
        ////
        //// Precent:
        //// 
        //// L   P          C             R
        //// 
        //// L - left side
        //// R - right side
        //// C - center
        //////////////////////////

        //if (positionPrev.x > (positionCenter.x * (percentSwap)))
        //{
        //    //Debug.Log(string.Format(
        //    //    "<color=orange>Coords:</color> positionCenter.x={0} positionCurrent.x={1}  current.sprite.name = {2}",
        //    //    positionCenter.x,
        //    //    positionCurrent.x,
        //    //    current.sprite.name
        //    //    ));


        //    Decrement();
        //    current.sprite = SetImage(currentImage);
        //    next.sprite = SetImage(currentImage + 1);
        //    prev.sprite = SetImage(currentImage - 1);




        //}
        #endregion

        float mult = 100.0f;
        Vector3 mousePosCurrWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        float delta = (mousePosCurrWorld.x - mousePos.x) * mult;
        delta = (delta > .0f || delta < -0.001f ? sizeSwap / delta : .0f);
        delta = (delta > 1.0f ? 1.0f : delta);
        delta = (delta < -1.0f ? -1.0f : delta);

        //Debug.Log("<color=red> if swipe:</color> " + string.Format("delta = {0}; precentSwap = {1};",delta, percentSwap));
        if((delta>0? delta: -delta) > percentSwap)
        {
            //next - to left
            //prev - to right

            if (delta > 0)
            {
                //right
                //Debug.Log("<color=red> Swipe To </color><color=green>Right</color>: " + string.Format("delta = {0}; currentImage = {1};", delta, currentImage));
                Decrement();
                //Debug.Log("<color=red> Swipe To </color><color=green>Right</color>: " + string.Format("delta = {0}; currentImage = {1};", delta, currentImage));

                next.sprite = SetImage(currentImage - 1);
            }
            else
            {
                //left 
                //Debug.Log("<color=red> Swipe To </color><color=green>Left</color>: " + string.Format("delta = {0}; currentImage = {1};", delta, currentImage));
                Increment();
                //Debug.Log("<color=red> Swipe To </color><color=green>Left</color>: " + string.Format("delta = {0}; currentImage = {1};", delta, currentImage));


                next.sprite = SetImage(currentImage + 1);
            }
        }
        current.sprite = SetImage(currentImage);
        nextObj.SetActive(false);
        current.color = (new Color(1f, 1f, 1f, 1f));


        //Vector3 mousePosCurr = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        currentObj.transform.position = new Vector3(
            gameObject.transform.position.x,
            nextObj.transform.position.y,
            gameObject.transform.position.z
            );

        //prevObj.transform.position = new Vector3(
        //    gameObject.transform.position.x - Get_Width(this),
        //    nextObj.transform.position.y,
        //    gameObject.transform.position.z
        //    );
        //current.color = (new Color(1f, 1f, 1f, 1f));
        //next.color = (new Color(1f, 1f, 1f, .0f));
        //prev.color = (new Color(1f, 1f, 1f, 1f));

        


    }


    public void OnDrag(PointerEventData eventData)
    {
        //Vector3 mousePosCurr = Input.mousePosition;

        Vector3 mousePosCurrWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //Debug.Log(string.Format(
        //    "<color=brown> Position: </color> X[{0}] Y[{1}]",
        //    positionCenter.x,
        //    positionCenter.y
        //    ));
        //Debug.Log(string.Format(
        //     "<color=gray> Position: </color> X[{0}] Y[{1}]",
        //     mousePosCurr.x,
        //     mousePosCurr.y
        //     ));

        //next - to left
        //prev - to right
        nextObj.SetActive(false);
        prevObj.SetActive(false);
        current.color = (new Color(1f, 1f, 1f, 1f));
        next.color = (new Color(1f, 1f, 1f, 1f));
        float mult = 100.0f;
        if (mousePosCurrWorld.x > mousePos.x)
        {
            //swipe to right
            currentObj.transform.position = new Vector2(
            currPos.x + (mousePosCurrWorld.x - mousePos.x) * mult,
            currPos.y);
            //Debug.Log("Right delta positions: " + (mousePosCurrWorld.x - mousePos.x) * mult);

            next.sprite = SetImage(currentImage - 1);
            nextObj.SetActive(true);

            float delta = (mousePosCurrWorld.x - mousePos.x) * mult;
            delta = delta * (delta < 0 ? -1.0f : 1.0f);
            delta = (delta > .0f ? sizeSwap / delta : 1.0f);
            delta = (delta > 1.0f ? 1.0f : delta);

            current.color = (new Color(1f, 1f, 1f, delta));


        }
        else
        {
            //swipe to left
            currentObj.transform.position = new Vector2(
            currPos.x + (mousePosCurrWorld.x - mousePos.x) * mult,
            currPos.y);
            //Debug.Log("Left delta positions: " + (mousePosCurrWorld.x - mousePos.x) * mult);

            next.sprite = SetImage(currentImage + 1);
            nextObj.SetActive(true);

            float delta = (mousePosCurrWorld.x - mousePos.x) * mult;
            delta = delta * (delta < 0 ? -1.0f : 1.0f);
            delta = (delta > .0f ? sizeSwap / delta : 1.0f);
            delta = (delta > 1.0f ? 1.0f : delta);

            current.color = (new Color(1f, 1f, 1f, delta));
        }

        #region PrevCode dont use it
        //if(mousePosCurrWorld.x <= positionCenter.x)
        //{
        //    prevObj.transform.position = new Vector2(
        //        gameObject.transform.position.x - Get_Width(prevObj),
        //        prevObj.transform.position.y
        //        );

        //    currentObj.transform.position = new Vector2(
        //    mousePosCurr.x,
        //    currentObj.transform.position.y);

        //    float blur = 1.0f - (gameObject.transform.position.x - mousePosCurrWorld.x) / gameObject.transform.position.x;
        //    current.color = (new Color(1f, 1f, 1f, blur));
        //    Debug.Log("<color=orange>Blur:</color> " + blur.ToString());
        //    Debug.Log(string.Format(
        //        "<color=orange>Coords:</color> positionCenter.x={0} mousePosCurrWorld.x={1} ",
        //        positionCenter.x,
        //        mousePosCurrWorld.x
        //        ));
        //    /* 
        //     * blur      anti-blur
        //     * 1     -   0 
        //     * 0.98  -   0.02
        //     * 0.5   -   0.5
        //     * 0.36  -   0.64
        //     * 0     -   1
        //     * 
        //     * y = 1-x where x == blur; y == anti-blur
        //     * 
        //     * x=1:    y = 1-1 = 0
        //     * x=0.98: y = 1-0.98 = 0.02
        //     * x=0.5:  y = 1-0.5 = 0.5
        //     * x=0.36: y = 1-0.98 = 0.02
        //     * x=0:    y = 1-0 = 1
        //     */
        //    nextObj.SetActive(true);
        //    next.color = (new Color(1f, 1f, 1f, 1.0f - blur));
        //}
        //else
        //{
        //    currentObj.transform.position = new Vector2(
        //    gameObject.transform.position.x,
        //    nextObj.transform.position.y );

        //    prevObj.transform.position = new Vector2(
        //    mousePosCurr.x - currentObj.transform.position.x / 2.0f,
        //    currentObj.transform.position.y);

        //    float blur = 1.0f + (prevObj.transform.position.x - gameObject.transform.position.x) / gameObject.transform.position.x;

        //    if (blur > 1.0f)
        //        blur = 1.0f;

        //    //Debug.Log("<color=orange>Blur:</color> " + blur.ToString());
        //    //Debug.Log(string.Format(
        //    //    "<color=orange>Coords:</color> gameObject.x={0} mousePosCurrWorld.x={1} prevObj.x={2}",
        //    //    gameObject.transform.position.x,
        //    //    mousePosCurrWorld.x,
        //    //    prevObj.transform.position.x
        //    //    ));

        //    prev.color = (new Color(1f, 1f, 1f, blur));
        //    current.color = (new Color(1f, 1f, 1f, (1f - blur)));
        //    nextObj.SetActive(false);
        //}
        #endregion
    }
}

[Serializable] 
public class PerfectImage
{
    [Tooltip("Select one of type. Prior - sprite>Local>URL")]
    public Sprite img;
    [Tooltip("Select one of type. Prior - sprite>Local>URL")]
    public string URL;
    [Tooltip("Select one of type. Prior - sprite>Local>URL")]
    public string localPath;
    public enum ReferenceType
    {
        URL,
        Path,
        Bmp,
    }
    public PerfectImage()
    {

    }
    public Sprite getImage(int width = 800, int height = 600)
    {
        if(this.img != null)
        {
            
            return this.img;
        }
        Texture2D texture = Texture2D.whiteTexture;
        texture.Resize(width, height);
        return Sprite.Create(texture, new Rect(0, 0, width, height), new Vector2(0, 0));
    }
    public void Load()
    {
        if(this.img == null)
        {
            if(localPath != null || URL != null)
            {
                Load(
                    (localPath != null ? localPath:URL)
                    );
            }
            else
            {
                Texture2D texture = Texture2D.whiteTexture;
                texture.Resize(800, 600);
                Load(Sprite.Create(texture, new Rect(0, 0, 800, 600), new Vector2(0, 0)));
            }
        }
    }
    public void Load(string url_path)
    {
        if (url_path.Contains("http"))
            DownloadImage(url_path);
        else
            LoadFromResource(url_path);
        
    }

    public void Load(Sprite sprite)
    {
        this.img = sprite;
    }
   
    private void LoadFromResource(string url_path)
    {
        //Debug.Log("<color=pink>IMAGE</color> try to load " + url_path);
        img = Resources.Load<Sprite>(url_path) as Sprite;
    }

    private IEnumerator DownloadImage(string url)
    {
        // Start a download of the given URL
        using (WWW www = new WWW(url))
        {
            // Wait for download to complete
            yield return www;

            // assign texture
            img = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
        }
    }
}