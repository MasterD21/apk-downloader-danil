﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GideGalleryLoader : MonoBehaviour {

    public GameObject gallery;
    public string ParamName;
	// Use this for initialization
	void Awake () {

        string[] imgsL = GlobalStrings.GetString(ParamName).Split(';');

        PerfectImage[] imgs = new PerfectImage[imgsL.Length];
        for(int i = 0; i< imgsL.Length; i++)
        {

            imgs[i] = new PerfectImage();
            imgs[i].localPath = (imgsL[i]);
        }
        gallery.GetComponent<PerfectGallery>().images = imgs;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
