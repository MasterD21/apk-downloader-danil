﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowBannerCenterBig : MonoBehaviour {

	// Use this for initialization
	void Start () {
        ADManager.Instance.ShowBaner(admob.AdSize.MediumRectangle, admob.AdPosition.MIDDLE_CENTER, 5);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
