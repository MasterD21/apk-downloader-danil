﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;




[System.Serializable]
public class Save
{
    public List<int> rewards = new List<int>();
    public DateTime lastRoll;
    public bool AdditionalRoll;
    public void InitNew()
    {
        rewards = new List<int>();
        lastRoll = DateTime.Now;
        lastRoll = lastRoll.AddHours(-12);
        AdditionalRoll = false;
    }
}

public static class SaveManager
{
    public static Save save;
    private static string saveStr = "appdata.save";
    public static void SaveGame()
    {

        // 2
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + saveStr);
        bf.Serialize(file, save);
        file.Close();

       
        //Debug.Log("Game Saved");
    }

    public static void LoadGame()
    {
        // 1
        if (File.Exists(Application.persistentDataPath + "/" + saveStr))
        {

            // 2
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + saveStr, FileMode.Open);
            save = (Save)bf.Deserialize(file);
            file.Close();

            //Debug.Log("Game Loaded");
        }
        else
        {
            save = new Save();
            save.InitNew();
            //Debug.Log("No game saved!");

        }
    }
}