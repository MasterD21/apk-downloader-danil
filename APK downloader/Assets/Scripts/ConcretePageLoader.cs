﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ConcretePageLoader : MonoBehaviour {
    public Text LableName;
    public Button GuideButton;
    public Button DescriptionButton;
    public Button DownloadButton;
    // Use this for initialization
    void Start () {

        ADManager.Instance.ShowBaner(admob.AdSize.MediumRectangle, admob.AdPosition.MIDDLE_CENTER, 5);
        //Debug.Log("<color=blue>IdCurrent:</color> " + GlobalStrings.idCurrent.ToString());

        LableName.text = GlobalStrings.elementsSources[GlobalStrings.idCurrent].Name; //GlobalStrings.GetString("ListPageName");
        (GuideButton.GetComponentInChildren<Text>()).text = GlobalStrings.GetString("ConcretePageButtonGuide");
        (DescriptionButton.GetComponentInChildren<Text>()).text = GlobalStrings.GetString("ConcretePageButtonDescription");
        (DownloadButton.GetComponentInChildren<Text>()).text = GlobalStrings.GetString("ConcretePageButtonDownload");

    }
	
    public void OpenGuide()
    {
        new ChangeScene().changeScreen(GlobalStrings.elementsSources[GlobalStrings.idCurrent].Guide + "Guide");
    }

    public void Dowbload()
    {
        //string[] path_split1 = GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk.Split('/');
        //string path1 = string.Format("{0}/{1}.rar", Application.persistentDataPath, path_split1[path_split1.Length - 1]);
        //GlobalStrings.elementsSources[GlobalStrings.idCurrent].Name = path1;
        if (GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk.Contains("http"))
        {
            Application.OpenURL(GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk);
        }
        else
        {
            #region Download file from asset to local
            // Put your file to "YOUR_UNITY_PROJ/Assets/StreamingAssets"
            // example: "YOUR_UNITY_PROJ/Assets/StreamingAssets/db.bytes"
            string realPath = Application.persistentDataPath + "/" + GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk;
            if (!File.Exists(realPath))
            {
                string dbPath = "";

                // Android
                string oriPath = Path.Combine(Application.streamingAssetsPath, GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk);

                // Android only use WWW to read file
                WWW reader = new WWW(oriPath);
                while (!reader.isDone) { }

                File.WriteAllBytes(realPath, reader.bytes);

                dbPath = realPath;
            }
            #endregion


            //string path_file = "Assets/Resources/"+ GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk + ".zip";
            //string filePath = Path.Combine(Application.streamingAssetsPath, GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk + ".zip");
            ////Read the text from directly from the test.txt file
            ////BinaryReader reader = new BinaryReader(File.ReadAllBytes(path_file));
            //byte[] array = File.ReadAllBytes(filePath);//ReadAllBytes(reader);
            ////reader.Close();

            /////////////////////////
            ////TextAsset textAsset = Resources.Load(GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk, typeof(TextAsset)) as TextAsset;
             
            //string[] path_split = GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk.Split('/');
            //string path = string.Format("{0}/{1}.zip", Application.persistentDataPath, path_split[path_split.Length - 1]);//Application.persistentDataPath
            //GlobalStrings.elementsSources[GlobalStrings.idCurrent].Name = GlobalStrings.elementsSources[GlobalStrings.idCurrent].Apk;
            ////System.IO.File.WriteAllBytes(path, textAsset.bytes);

            //using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.Create)))
            //{
            //    writer.Write(array);
            //    writer.Close();
            //}



            Application.OpenURL(realPath);
        }
    }
    
    public static string GetDownloadFolder()
    {
        string[] temp = (Application.persistentDataPath.Replace("Android", "")).Split(new string[] { "//" }, System.StringSplitOptions.None);

        return (temp[0] + "/Download");
    }
    // Update is called once per frame
    void Update () {
		
	}
}
