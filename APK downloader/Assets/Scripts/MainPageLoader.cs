﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainPageLoader : MonoBehaviour {

    public Button btnToList;
    public Button btnToRewards;
    public Text lableMainPage;

    public void Awake()
    {
        ADManager.Instance.ShowBaner(admob.AdSize.MediumRectangle, admob.AdPosition.MIDDLE_CENTER, 75);
        lableMainPage.text = GlobalStrings.GetString("MainPageName");
        btnToList.GetComponentInChildren<Text>().text = GlobalStrings.GetString("MainPageButtonList");
        btnToRewards.GetComponentInChildren<Text>().text = GlobalStrings.GetString("MainPageButtonReward");
    }
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
