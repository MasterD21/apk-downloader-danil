﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;

public class Globals : MonoBehaviour {
    public TextAsset inFile;
    void Awake()
    {
        GlobalStrings.Load(inFile);
        SaveManager.LoadGame();

        if (SaveManager.save.rewards.Count >= 1)
        {
            try
            {
                GlobalStrings.idReward = GlobalStrings.rewardElements.IndexOf(GlobalStrings.rewardElements.First(x => x.id == SaveManager.save.rewards[SaveManager.save.rewards.Count - 1]));
            }
            catch (Exception)
            {

                GlobalStrings.idReward = 0;
            }
        }
        else
        {
            GlobalStrings.idReward = -1;
        }
        //Debug.Log("<color=green>Fatal error:</color> " + GlobalStrings.GetString("MainPageName"));
        //ADManager.Instance.ShowBaner(admob.AdSize.MediumRectangle, admob.AdPosition.MIDDLE_CENTER, 5, "TestBanner");
        //GameObject gallery = GameObject.Find("PerfectGallery");
        //PerfectImage[] imgs = new PerfectImage[5];
        //imgs[0] = new PerfectImage();
        //imgs[1] = new PerfectImage();
        //imgs[2] = new PerfectImage();
        //imgs[3] = new PerfectImage();
        //imgs[4] = new PerfectImage();
        //imgs[0].localPath = ("Sprites/gallery/gallery001");
        //imgs[1].localPath = ("Sprites/gallery/gallery002");
        //imgs[2].localPath = ("Sprites/gallery/gallery003");
        //imgs[3].localPath = ("Sprites/gallery/gallery004");
        //imgs[4].localPath = ("Sprites/gallery/gallery006");
        //gallery.GetComponent<PerfectGallery>().images = imgs;
    }
    
}
public static class GlobalStrings
{
    public static HashSet<StringByName> stringByNames = new HashSet<StringByName>(new StringByNameComparer());
    public static List<ElementsSource> elementsSources = new List<ElementsSource>();
    public static List<RewardElement> rewardElements = new List<RewardElement>();
    public static int idCurrent = 0;
    public static bool isInfo = true;
    public static int idReward = 0;
    // TODO: here values of items (in list)

    //public GlobalStrings()
    //{
    //    string xml;
    //}
    public static void Load(TextAsset inFile)
    {
        //Debug.Log("<color=red>Fatal error:</color> Load");

        //Debug.Log("<color=red>Fatal error:</color> " + inFile.text);
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.LoadXml(inFile.text);

        // получим корневой элемент
        XmlElement xRoot = xmldoc.DocumentElement;

        XmlNode strings = xRoot.ChildNodes.Item(0);
        foreach(XmlNode str in strings.ChildNodes)
        {
            string name = str.Attributes.GetNamedItem("name").Value;
            string value = str.InnerText;
            //Debug.Log("<color=red>Fatal error:</color> " + name);
            //Debug.Log("<color=red>Fatal error:</color> " + value);
            stringByNames.Add(new StringByName(name, value));
        }


        //int tmp_index = 0;
        XmlNode pizdas = xRoot.ChildNodes.Item(1);
        foreach (XmlNode jopa in pizdas.ChildNodes)
        {
            // here we have concrete JOPA
            string name = "";
            string description = "";
            string apk = "";
            string link = "";
            string guide = "";
            string imgs = "";
            foreach (XmlNode elem in jopa.ChildNodes)
            {
                if (elem.Name == "name") { name = elem.InnerText; }
                if (elem.Name == "description") { description = elem.InnerText; }
                if (elem.Name == "img") { imgs = elem.InnerText; }
                if (elem.Name == "apk") { apk = elem.InnerText; }
                if (elem.Name == "link") { link = elem.InnerText; }
                if (elem.Name == "guide") { guide = elem.InnerText; }
            }

            elementsSources.Add(new ElementsSource(name, description, guide, imgs, apk, link));
            //elementsSources.Add(new ElementsSource(tmp_index.ToString() + name, description, guide, imgs, apk, link));
            //tmp_index++;
            //elementsSources.Add(new ElementsSource(tmp_index.ToString() + name, description, guide, imgs, apk, link));
            //tmp_index++;
            //elementsSources.Add(new ElementsSource(tmp_index.ToString() + name, description, guide, imgs, apk, link));
            //tmp_index++;
            //Debug.Log("<color=red>Fatal error:</color> " +
            //    string.Format("\n name: {0}\n description: {1}\n imgs: {2}\n apk: {3}\n link: {4}\n info: {5}\n guide: {6}",
            //    name, description, imgs, apk, link, "pass", guide));


        }

        XmlNode rewards = xRoot.ChildNodes.Item(2);
        foreach (XmlNode reward in rewards.ChildNodes)
        {
            // herereward`s elements

            int id = 0;
            string name = "";
            string img = "";
            string file = "";
            string link = "";
            foreach (XmlNode elem in reward.ChildNodes)
            {
                if (elem.Name == "id") { id = Convert.ToInt32(elem.InnerText); }
                if (elem.Name == "name") { name = elem.InnerText; }
                if (elem.Name == "file") { file = elem.InnerText; }
                if (elem.Name == "link") { link = elem.InnerText; }
                if (elem.Name == "image") { img = elem.InnerText; }
            }
            rewardElements.Add(new RewardElement(id, name, (file.Length > 2 ? file : link), img));


        }
    }
    public static ElementsSource GetElement(string name)
    {
        return elementsSources.FirstOrDefault(x => x.Name == name);
    }

    //public static ElementsSource GetElement(int key)
    //{
    //    if (key < elementsSources.Count)
    //        return elementsSources[key];
    //    else
    //        return new ElementsSource("null", "null", "null", "null", "null");
    //}
    public static string GetString(string name)
    {
        
        return stringByNames.FirstOrDefault(x => x.Name == name).Value;
    }
    //public static string GetString(int key)
    //{
    //    if (key < stringByNames.Count)
    //        return stringByNames[key];
    //    else
    //        return new StringByName("null","null");
    //}
}

public class RewardElement
{
    public int id;
    public string name;
    public PerfectImage img;
    public string file_link;

    public RewardElement(int id, string name, string file_link, string img)
    {
        this.id = id;
        this.name = name;
        this.file_link = file_link;
        this.img = new PerfectImage();
        this.img.localPath = img;
        this.img.Load();
    }
}
public class StringByName
{
    public string Name { get; set; }
    public string Value { get; set; }

    public StringByName(string name, string value)
    {
        this.Name = name;
        this.Value = value;
    }

    //public static bool operator == (StringByName obj1, StringByName obj2)
    //{
    //    if (obj1 == null && obj2 == null)
    //        return true;
    //    if (obj1 == null || obj2 == null)
    //        return false;
    //    return (obj1.Name == obj2.Name);
    //}
    //public static bool operator !=(StringByName obj1, StringByName obj2)
    //{
    //    return !(obj1.Name == obj2.Name);
    //}
    public override bool Equals(object obj)
    {
        return (this.Name == obj as string);
    }
    public bool Equals(StringByName obj)
    {
        return (this.Name == obj.Name);
    }
    public override string ToString()
    {
        return this.Name;
    }
    public override int GetHashCode()
    {
        return this.Name == null ? 0 : this.Name.GetHashCode();
    }
    public string this [int key]
    {
        get
        {
            return (key == 0) ? this.Name : this.Value;
        }
        set
        {
            if (key == 0)
                this.Name = value;
            else
                this.Value = value;
        }
    }

}

public class StringByNameComparer : IEqualityComparer<StringByName>
{
    public bool Equals(StringByName x, StringByName y)
    {
        if (x == null && y == null) return true;
        if (x == null || y == null) return false;
        return x.Name == y.Name;
    }

    public int GetHashCode(StringByName obj)
    {
        if (obj == null) return 0;
        return obj.Name == null ? 0 : obj.Name.GetHashCode();
    }
}

public class ElementsSource
{
    private string name;
    private string decription;
    private string apk;
    private string link;
    private string guide;
    private string img;

    private static string default_img = "";

    public ElementsSource(string name, string decription, string guide, string img_xml,  string apk="", string link = "")
    {
        this.Name = name;
        this.Decription = decription;
        this.Guide = guide;
        this.Apk = apk;
        this.Link = link;
        this.Images = img_xml;
    }



    public override bool Equals(object obj)
    {
        return (this.Name == obj as string);
    }
    public bool Equals(ElementsSource obj)
    {
        return (this.Name == obj.Name);
    }
    public override string ToString()
    {
        return this.Name;
    }
    public override int GetHashCode()
    {
        return this.Name == null ? 0 : this.Name.GetHashCode();
    }

    public string Images
    {
        get { return this.img; }
        set { this.img = value; }
    }

    public string Name
    {
        get { return this.name; }
        set { this.name = value; }
    }
    public string Decription
    {
        get { return this.decription; }
        set { this.decription = value; }
    }
    public string Guide
    {
        get { return this.guide; }
        set { this.guide = value; }
    }
    public string Apk
    {
        get {
            if (this.apk.Length < 2)
            {
                if (this.link.Length < 2)
                {
                    return "https://www.google.com";
                }
                else
                {
                    return this.link;
                }
            }
            else
            {
                return this.apk;
            }
        }
        set { this.apk = value; }
    }
    public string Link
    {
        get {
            if (this.link.Length < 2)
            {
                return "https://www.google.com";
            }
            else
            {
                return this.link;
            }
        }
        set { this.link = value; }
    }
}


public class ElementsSourceComparer : IEqualityComparer<ElementsSource>
{
    public bool Equals(ElementsSource x, ElementsSource y)
    {
        if (x == null && y == null) return true;
        if (x == null || y == null) return false;
        return x.Name == y.Name;
    }

    public int GetHashCode(ElementsSource obj)
    {
        if (obj == null) return 0;
        return obj.Name == null ? 0 : obj.Name.GetHashCode();
    }
}
