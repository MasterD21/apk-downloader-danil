﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListPageLoader : MonoBehaviour {

    public GameObject ListContainer;
    public GameObject ListElement;
    public Text LablePage;
	// Use this for initialization
	void Start () {
        //ChangePageLable
        LablePage.text = GlobalStrings.GetString("ListPageName");
        //----------------------------
        // DisplayAD
        ADManager.Instance.ShowBaner(admob.AdSize.Banner, admob.AdPosition.BOTTOM_CENTER, 5);
        //-----------------------------
        //get total height (summ of heights)
        float elemHeight = (float)PerfectGallery.Get_Height(ListElement);
        float total_height = (float)GlobalStrings.elementsSources.Count * elemHeight + elemHeight * 1.5f;
        //setHeight to container

        ListContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(ListContainer.GetComponent<RectTransform>().sizeDelta.x, total_height);
        ListContainer.GetComponent<RectTransform>().anchoredPosition = new Vector3(ListContainer.GetComponent<RectTransform>().anchoredPosition.x, .0f);
        //ListContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(total_height, PerfectGallery.Get_Width(ListContainer));

        for (int i =0; i < GlobalStrings.elementsSources.Count; i++)
        {
            GameObject Item = Instantiate(ListElement, ListContainer.transform);
            Item.name = string.Format("ListItem{0}", i);
            Item.transform.position = new Vector2(0, (1-i)* elemHeight - elemHeight * .5f);

            Button button = Item.GetComponentInChildren<Button>(true);
            if (button != null)
            {
                button.name = i.ToString();
                button.onClick.AddListener(delegate () { OpenConcreteItem(button.name); });
            }

            Text text = Item.GetComponentsInChildren<Text>(true)[1];
            text.text = GlobalStrings.elementsSources[i].Name;

        }
    }
    public void OpenConcreteItem(string idN)
    {
        int id = System.Convert.ToInt32(idN);
        new ChangeScene().changeScreen("ConcreteItem");
        GlobalStrings.idCurrent = id;
    }
}
