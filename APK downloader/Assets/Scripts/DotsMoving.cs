﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotsMoving : MonoBehaviour {
    Text txt;
    // Next update in second
    private int nextUpdate = 1;
    private bool enabled_load = true;
    public int limit = 10;
    public GameObject loadingDot;
    public GameObject BtnContinue;
    private int dotCount = 1;

    private void Awake()
    {
        BtnContinue.SetActive(false);
    }
    // Use this for initialization
    void Start () {
        txt = loadingDot.GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        // If the next update is reached
        if (Time.time >= nextUpdate && enabled_load)
        {
            //Debug.Log(Time.time + ">=" + nextUpdate);
            // Change the next update (current second+1)
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            // Call your fonction
            UpdateEverySecond();
        }

        if (nextUpdate > limit)
        {
            enabled_load = false;

            BtnContinue.SetActive( true);
            
            gameObject.SetActive(false);
        }
    }

    // Update is called once per second
    void UpdateEverySecond()
    {
        if (dotCount > 3)
        {
            dotCount = 1;
        }

        txt.text = new string('.', dotCount);

        dotCount++;

    }
}
