﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoGuidePageLoader : MonoBehaviour {

    public GameObject gallery;
    public Text text;
	// Use this for initialization
	void Awake () {
        text.text = GlobalStrings.elementsSources[GlobalStrings.idCurrent].Decription;

        //Debug.Log(GlobalStrings.elementsSources[GlobalStrings.idCurrent].Images);
        string[] imgsL = GlobalStrings.elementsSources[GlobalStrings.idCurrent].Images.Split(';');
        //Debug.Log(GlobalStrings.elementsSources[GlobalStrings.idCurrent].Images);
        PerfectImage[] imgs = new PerfectImage[imgsL.Length];
        for (int i = 0; i < imgsL.Length; i++)
        {

            imgs[i] = new PerfectImage();
            imgs[i].localPath = (imgsL[i]);
        }
        gallery.GetComponent<PerfectGallery>().images = imgs;

    }

    private void Start()
    {

        ADManager.Instance.ShowBaner(admob.AdSize.Banner, admob.AdPosition.BOTTOM_CENTER, 5);
    }
    // Update is called once per frame
    void Update () {
		
	}
}
